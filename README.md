<h1 align="center">Task Manager app</h1>

I built this small CURD project to combine React JS and Django Rest Framework. <br></br> 

# Built With 
### <p style="font-size:22px"> &#9654; front-end</p> 
- HTML & CSS
- Bootstarp CSS
- React JS

### <p style="font-size:22px"> &#9654; back-end</p> 
- Django Rest Framework

### <p style="font-size:22px"> &#9654; database</p> 
- SQL light
<br></br> 

# Author
Skartados Theodoros