import { Fragment, useEffect, useState, useCallback } from "react";
import useHttp from "./hooks/use-http";
import AddTask from "./components/AddTask/AddTask";
import TaskList from "./components/TaskList/TaskList";

function App() {
  const { sendRequest } = useHttp();
  const [tasks, setTasks] = useState([]);
  const [update, setUpdate] = useState(false);

  useEffect(() => {
    sendRequest(
      {
        url: "http://localhost:8000/api/tasks/",
      },
      (data) => setTasks(data)
    );
    return () => {
      console.log(update);
      setUpdate(false);
    };
  }, [update, sendRequest]);

  const fetchData = useCallback(() => {
    setUpdate(true);
  }, []);

  return (
    <Fragment>
      <AddTask onFetch={fetchData} />
      <TaskList tasks={tasks} onFetch={fetchData} />
    </Fragment>
  );
}

export default App;
