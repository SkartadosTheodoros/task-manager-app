import { useState, Fragment } from "react";
import useHttp from "../../hooks/use-http";
import TaskForm from "./TaskForm";

const AddTask = (props) => {
  const {sendRequest } = useHttp();
  const [modal, setModal] = useState(false);

  const toggleHandler = () => {
    setModal(!modal);
  };

  const addTaskHandler = (taskObj) => {
    sendRequest(
      {
        url: "http://localhost:8000/api/tasks/",
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: {
          title: taskObj.title,
          description: taskObj.description,
          completed: taskObj.completed,
        },
      }
    );

    props.onFetch()
  };

  return (
    <Fragment>
      <div className="header text-center pt-5">
        <h1>Task Manager</h1>
        <button className="btn btn-primary mt-3" onClick={toggleHandler}>
          Create a Task
        </button>
      </div>
      <div>
        <TaskForm
          modal={modal}
          toggle={toggleHandler}
          addTask={addTaskHandler}
        />
      </div>
    </Fragment>
  );
};

export default AddTask;
