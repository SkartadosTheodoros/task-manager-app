import { useState } from "react";
import useInput from "../../hooks/use-input";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

const TaskForm = (props) => {
  const {
    value: title,
    valueIsValid: titleIsValid,
    valueIsInvalid: titleIsInvalid,
    changeInputValueHandler: inputTitleChangeHandler,
    blurInputValueHandler: blurTitleChangeHandler,
    reset: resetTitle,
  } = useInput((value) => value.trim() !== "");

  const {
    value: description,
    valueIsValid: descriptionIsValid,
    valueIsInvalid: descriptionIsInvalid,
    changeInputValueHandler: inputDescriptionChangeHandler,
    blurInputValueHandler: blurDescriptionChangeHandler,
    reset: resetDescription,
  } = useInput((value) => value.trim() !== "");

  const [completed, setCompleted] = useState(false);
  const completedChangeHandler = (event) => {
    setCompleted(event.target.checked);
  };

  let formIsValid = false;
  if (titleIsValid && descriptionIsValid) {
    formIsValid = true;
  }

  const submitHandler = (event) => {
    event.preventDefault();

    const taskObj = {
      title: title,
      description: description,
      completed: completed,
    };

    props.addTask(taskObj);
    props.toggle();

    resetTitle();
    resetDescription();
    setCompleted(false);
  };

  return (
    <Modal isOpen={props.modal} toggle={props.toggle}>
      <ModalHeader toggle={props.toggle}>Create a Task</ModalHeader>

      <ModalBody>
        <form onSubmit={submitHandler}>
          <div className={`form-group pb-3 ${titleIsInvalid ? "invalid" : ""}`}>
            <label htmlFor="title">Title</label>
            <input
              className="form-control"
              id="title"
              type="text"
              name="title"
              value={title}
              onChange={inputTitleChangeHandler}
              onBlur={blurTitleChangeHandler}
              placeholder="Enter a Task Title"
            />
          </div>

          <div
            className={`form-group pb-3 ${
              descriptionIsInvalid ? "invalid" : ""
            }`}
          >
            <label htmlFor="description">Description</label>
            <textarea
              className="form-control"
              id="description"
              name="description"
              type="text"
              rows="5"
              value={description}
              onChange={inputDescriptionChangeHandler}
              onBlur={blurDescriptionChangeHandler}
              placeholder="Enter a Task Description"
            />
          </div>

          <div className="form-check pt-2">
            <label htmlFor="completed" className="form-check-label">
              Completed
            </label>
            <input
              className="form-check-input"
              type="checkbox"
              id="completed"
              name="completed"
              value={completed}
              onChange={completedChangeHandler}
            />
          </div>
        </form>
      </ModalBody>

      <ModalFooter>
        <Button disabled={!formIsValid} color="primary" onClick={submitHandler}>
          Add Task
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default TaskForm;
