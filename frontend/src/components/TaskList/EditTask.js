import { useState } from "react";
import useInput from "../../hooks/use-input";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

const text_validator = (value) => value.trim() !== "";

const EditTask = (props) => {
  const {
    value: title,
    valueIsValid: titleIsValid,
    valueIsInvalid: titleIsInvalid,
    changeInputValueHandler: inputTitleChangeHandler,
    blurInputValueHandler: blurTitleChangeHandler,
    reset: resetTitle,
  } = useInput(text_validator, props.task.title);

  const {
    value: description,
    valueIsValid: descriptionIsValid,
    valueIsInvalid: descriptionIsInvalid,
    changeInputValueHandler: inputDescriptionChangeHandler,
    blurInputValueHandler: blurDescriptionChangeHandler,
    reset: resetDescription,
  } = useInput(text_validator, props.task.description);

  const [completed, setCompleted] = useState(props.task.completed);
  const completedChangeHandler = (event) => {
    setCompleted(event.target.checked);
  };

  let formIsValid = false;
  if (titleIsValid && descriptionIsValid) {
    formIsValid = true;
  }

  const submitHandler = (event) => {
    event.preventDefault();

    const taskObj = {
      id: props.task.id,
      title: title,
      description: description,
      completed: completed,
    };

    props.editTask(taskObj);
    props.toggle();

    resetTitle();
    resetDescription();
    setCompleted(false);
  };

  return (
    <Modal isOpen={props.modal} toggle={props.toggle}>
      <ModalHeader toggle={props.toggle}>Create a Task</ModalHeader>

      <ModalBody>
        <form onSubmit={submitHandler}>
          <div className={`form-group pb-3 ${titleIsInvalid ? "invalid" : ""}`}>
            <label htmlFor="title">Title</label>
            <input
              className="form-control"
              id="title"
              type="text"
              name="title"
              value={title}
              onChange={inputTitleChangeHandler}
              onBlur={blurTitleChangeHandler}
              placeholder="Enter a Task Title"
            />
          </div>

          <div
            className={`form-group pb-3 ${
              descriptionIsInvalid ? "invalid" : ""
            }`}
          >
            <label htmlFor="description">Description</label>
            <textarea
              className="form-control"
              id="description"
              name="description"
              type="text"
              rows="5"
              value={description}
              onChange={inputDescriptionChangeHandler}
              onBlur={blurDescriptionChangeHandler}
              placeholder="Enter a Task Description"
            />
          </div>

          <div className="form-check pt-2">
            <label htmlFor="completed" className="form-check-label">
              Completed
            </label>
            <input
              className="form-check-input"
              type="checkbox"
              id="completed"
              name="completed"
              value={completed}
              checked={completed ? true : false}
              onChange={completedChangeHandler}
            />
          </div>
        </form>
      </ModalBody>

      <ModalFooter>
        <Button disabled={!formIsValid} color="primary" onClick={submitHandler}>
          Edit Task
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default EditTask;
