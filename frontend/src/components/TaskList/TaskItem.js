import { useState } from "react";
import EditTask from "./EditTask";

const TaskItem = (props) => {
  const colors = [
    {
      primaryColor: "#5D93E1",
      secondaryColor: "#ECF3FC",
    },
    {
      primaryColor: "#F9D288",
      secondaryColor: "#FEFAF1",
    },
    {
      primaryColor: "#5DC250",
      secondaryColor: "#F2FAF1",
    },
    {
      primaryColor: "#F48687",
      secondaryColor: "#FDF1F1",
    },
    {
      primaryColor: "#B964F7",
      secondaryColor: "#F3F0FD",
    },
  ];

  const [modal, setModal] = useState(false);

  const toggleHandler = () => {
    setModal(!modal);
  };

  const deleteTaskHandler = () => {
    props.deleteTask(props.task.id);
  };

  const editTaskHandler = (taskObj) => {
    props.editTask(taskObj);
  };

  return (
    <div className="card-wrapper m-3">
      <div
        className="card-top"
        style={{ "background-color": colors[props.index % 5].primaryColor }}
      ></div>

      <div className="task-holder">
        <span
          className="task-holder-header"
          style={{
            "background-color": colors[props.index % 5].secondaryColor,
          }}
        >
          {props.task.title}
        </span>

        <p className="mt-2">{props.task.description}</p>
      </div>

      <div className="d-flex justify-content-end m-3 px-1 py-2">
        <i
          className="task-button far fa-edit fa-lg px-2"
          style={{
            color: colors[props.index % 5].primaryColor,
          }}
          onClick={toggleHandler}
        ></i>
        <i
          className="task-button fas fa-trash-alt fa-lg "
          style={{
            color: colors[props.index % 5].primaryColor,
          }}
          onClick={deleteTaskHandler}
        ></i>
      </div>

      <EditTask
        modal={modal}
        toggle={toggleHandler}
        task={props.task}
        editTask={editTaskHandler}
      />
    </div>
  );
};

export default TaskItem;
