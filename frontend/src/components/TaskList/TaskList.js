import useHttp from "../../hooks/use-http";
import TaskItem from "./TaskItem";

const TaskList = (props) => {
  const { sendRequest } = useHttp();

  const deleteTaskHandler = async (id) => {
    await sendRequest({
      url: `http://localhost:8000/api/tasks/${id}/`,
      method: "DELETE",
    });
    props.onFetch();
  };

  const editTaskHandler = async (taskObj) => {
    console.log(taskObj);

    await sendRequest({
      url: `http://localhost:8000/api/tasks/${taskObj.id}/`,
      method: "PUT",
      body: {
        title: taskObj.title,
        description: taskObj.description,
        completed: taskObj.completed,
      },
      body: {
        taskObj
      },
    });
    props.onFetch();
  };

  return (
    <ul className="task-container">
      {props.tasks.map((task, index) => {
        return (
          <TaskItem
            key={task.id}
            task={task}
            index={index}
            deleteTask={deleteTaskHandler}
            editTask={editTaskHandler}
          />
        );
      })}
    </ul>
  );
};

export default TaskList;
