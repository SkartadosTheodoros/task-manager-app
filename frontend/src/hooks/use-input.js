import { useReducer } from "react";

let initialInputState = {
  enteredValue: "",
  enteredValueIsTouched: false,
};

const inputStateReducer = (state, action) => {
  if (action.type === "INPUT") {
    return {
      enteredValue: action.value,
      enteredValueIsTouched: state.enteredValueIsTouched,
    };
  }
  if (action.type === "BLUR") {
    return {
      enteredValue: state.enteredValue,
      enteredValueIsTouched: true,
    };
  }
  if (action.type === "INITIAL_VALUES") {
    return {
      enteredValue: action.value,
      enteredValueIsTouched: state.enteredValueIsTouched,
    };
  }
  if (action.type === "RESET") {
    return {
      enteredValue: "",
      enteredValueIsTouched: false,
    };
  }

  return initialInputState;
};

const useInput = (validator, initial_values=null) => { 
  if (initial_values != null) {
    initialInputState = {
      enteredValue: initial_values,
      enteredValueIsTouched: false,
    };
  }

  const [inputState, dispatch] = useReducer(
    inputStateReducer,
    initialInputState
  );

  const enteredValueIsValid = validator(inputState.enteredValue);
  const enteredValueIsInvalid =
    !enteredValueIsValid && inputState.enteredValueIsTouched;

  const changeInputValueHandler = (event) => {
    dispatch({ type: "INPUT", value: event.target.value });
  };

  const blurInputValueHandler = () => {
    dispatch({ type: "BLUR" });
  };

  const reset = () => {
    dispatch({ type: "RESET" });
  };

  return {
    value: inputState.enteredValue,
    valueIsValid: enteredValueIsValid,
    valueIsInvalid: enteredValueIsInvalid,
    changeInputValueHandler,
    blurInputValueHandler,
    reset,
  };
};

export default useInput;
